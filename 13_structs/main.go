package main

import (
	"fmt"
	"strconv"
)

// Define person strutct

type Person struct{
	firstName string
	lastName string
	city string
	gender string
	age int
}

//create method que recive el "objeto"

func (p Person) greet()string{
	return "Hello, my name is" + " " + p.firstName + " " + p.lastName + " " + "my age is" + " "+ strconv.Itoa(p.age) + " years old"
}

//getMarried (pointer reciever)
//methodo
func (p *Person) getMarried(spouseLastName string){
	if p.gender == "M"{
		return 
	}else{
		p.lastName = spouseLastName
	}
}

//cambiar algo de la estructura

func (p *Person) Cumpleaños(){
	p.age++
}

func (p *Person) CambiaNombre(newName string){
	p.firstName = newName
}

func main(){
	//init struct
	personax := Person{firstName:"Samantha",lastName:"Saint",city:"Boston",gender:"F",age:32}
	// fmt.Println(personax)
	// fmt.Println(personax.firstName)
	// personax.Cumpleaños()
	personax.getMarried("Millan")
	personax.CambiaNombre("Erika")
	fmt.Println(personax.greet())
	
}