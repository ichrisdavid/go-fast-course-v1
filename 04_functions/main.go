package main

import "fmt"

func greeting(name string) string{
	return "hello" + " " + name
}

func getSuma(num1, num2 float64) float64{
	return num1 + num2
}

func main() {
	//fmt.Println("hello world")
	//fmt.Println(greeting("chris david"))
	fmt.Println(getSuma(10.1,1.3))
}
