package main

import (
	"fmt"
)

func main(){

	//m := make(map[string]string)//array map
	nums := [3]int{1,2,3} //slice
	sum := 0
	//index := 0
	//metodo for, se le pasa el index de ser necesario, de no serlo se pasa _ range el arreglo que va a recorrer
	//example
	for _,num := range nums {
		sum += num
		
	}
	//fmt.Println(sum)
	//Aca se usa el index en el bucle
	for i,num := range nums {
		sum += num
		if i == 2 {
			//fmt.Println("paso por",i)
		}
	}

	kvs := make(map[string]string)// map
	kvs["a"] = "apple"
	kvs["b"] = "banana"
	//En este ejemplo, el for recorre el map pero el index es la llave del valor y no la posicion del arreglo
	variable := "b"
	for i,v := range kvs {
		if variable == i {
			fmt.Println(i + ":" +v)
		}
		
	}

	for k := range kvs {
		fmt.Println("key",k)
	}

	for i,c := range "go"{
		fmt.Println(i, c)
	}

	emails := map[string]string{"Bob":"bob@gmail.com","Sharon":"sharon@gmail.com"}
/*
	for k,v := range emails{
		fmt.Printf("%s :%s\n",k,v)
	} */
	//for with not key
	// si dejo el for solo con un valor me retornara siempre la key
	// si uso para la key el valor _,valor, me deolvera los valores
	for _,v := range emails{
		fmt.Println(v)
	}



	

}