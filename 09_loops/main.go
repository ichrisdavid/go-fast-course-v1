package main

import "fmt"

func main() {
	//long method
	i := 0
	for i <= 10 {
		//fmt.Println(i)
		i++
	}
	//long method
	for i = 0; i <= 10; i++{
		//fmt.Println(i)
	}

	//sacar los multiplos de 3 entre el 1 y el 100

	for i=0; i<100; i++{
		if i % 3 == 0{
			fmt.Println(i)
		}
	}
	
}
