package main

//https://www.golang-book.com/books/intro/8#:~:text=In%20Go%20a%20pointer%20is,value%20the%20pointer%20points%20to.

//ok just remenber * has two different values 1. define type pointer - 2. give me a value of a pointer
//the simbol & has two different values too 1. give me a adrress of a variable - 2. use in invocattion to any function with
//parameter address how i do with the examples



import "fmt"
//in this funcion i pass a a variable type pointer in other words i pass a address to a variable and inside to the function
//i get the slot to that address and put the value 0 inside
func zero(xPtr *int) {//pass address
	*xPtr = 0//new valye to the pointer
  }
func main() {
	x := 5
	zero(&x)//pass the address to the pointer variable
	fmt.Println(x) // x is still 5
  }

