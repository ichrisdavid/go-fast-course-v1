package main

import "fmt"
//diferencia entre = y :=, := es para asignar un valor se usa normalmente en variables enteros, strings
//pero para asignar por ejemplo el valor de arreglos se usa = como example["something"] = "algo"
func main() {
	name, edad, email := "Brad", 37, "brad@hotmail.com"
	//entonces se pueden declarar variables con el tipo nombre = a valor, pero son el := no hace falta mandarle el tipo
	fmt.Printf("%T\n", edad)
	fmt.Printf("%T\n", name)
	fmt.Printf("%T\n", email)
}
