package main

import "fmt"

func factorial(x int)int{
	//recursivo
	if x == 0{
		return 1
	}else {
		return x*factorial(x-1)

	}
}

func Fibo(numero int)int{
	//recursivo
	if numero == 0{
		return 0
 	}else if numero == 1{
	
		return 1
	
	}else{
		return Fibo(numero - 1) + Fibo(numero - 2)
	}
}

func main(){
	//fmt.Println(factorial(5))
	fmt.Println(Fibo(6))
}