package main

import "fmt"

func vals() (int, int) {
    return 3, 7
}
//funcion que retorna 2 valores, algo imposible en otros lenguajes de programacion
//al construir la funcion se le deben asignar el tipo de valores que devulve el return
func Test(num int,num2 int)(int,int){
	suma := num+num2
	resta := num-num2
	
	return suma,resta
}

func main() {

    a, b := vals()
    fmt.Println(a)
    fmt.Println(b)
	//al declarar la funcion se ṕuede recibir cualquiera de los 2 valores, el qu eno se quiere recibir se pone _
    f, _ := vals()
	fmt.Println(f)
	sumas,_ := Test(10,5)
	_,restas := Test(10,5)
	fmt.Println("valor de suma",sumas, "valor de resta", restas ) 
}