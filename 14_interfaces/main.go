package main

import (
	"fmt"
	"math"
)

// define interface
//Colecciones nombradas de signaturas de metodos

//se podria decir que la interfaz es el puente entre objetos que no tienen relacion alguna
//en este caso la interfaz Geometrica tiene 2 metodos, area() y perim()
type Geometrica interface {
	area() float64
	perim() float64
}

//creo 2 clases cuadraro y circulo y ambas tienen un mismo metodo llamado Area, una interfaz globaliza el metodo y
//permite que se comparta

type Cuadro struct { //defino el objeto con sus atributos de tipo float
	ancho, altura float64
}

type Circulo struct { //defino el objeto con sus atributos de tipo float
	x, y, radio float64
}

func (c Cuadro) area() float64 { //defino un methodo area que va a tener cuadro
	return c.ancho * c.altura
}

func (c Cuadro) perim() float64{
	return  2*c.ancho + 2*c.altura
}

func (c Circulo) perim() float64{
	return 2 * math.Pi * c.radio
}

func (c Circulo) area() float64 { // defino un metodo area de circulo
	return c.radio * c.radio * math.Pi
}



// func getArea(s Geometrica) (float64,float64) { //retorna 2 valores
// 	return s.area(),s.perim()
// }

func getMethodsGeometria(g Geometrica){
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perim())
}

func main() {

	cuadro := Cuadro{4.0,4.0}
	//circulo := Circulo{0,0,5}
	fmt.Println(cuadro.altura)
	//getMethodsGeometria(cuadro)

	// areaCuadrado,_ := getArea(circulo)
	// fmt.Printf("Area del cuadrao: %f\n",areaCuadrado)
}
