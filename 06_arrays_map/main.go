package main

import (
	"fmt"
	"strconv"
)

func main() {
	//declaro un arreglo map el cual, es con llave valor
	//defino el tipo de llave y el tipo de valor que tiene la llave
	m := make(map[string]string)

	m["nombre"] = "christian"
	m["edad"] = "10"
	m["apellido"] = "millan"
	edadStr := m["edad"]
	//fmt.Println(edadStr)
	if edad,i := strconv.ParseInt(edadStr,0,32); i == nil {
		fmt.Printf("%T\n",edad)
	}
	
	m["id"] = "12345"
	delete(m,"apellido")
	fmt.Println("map",m["edad"])
	//map no rquiere un tamaño fijo del arreglo, pero si requiere el tipo de llave y el tipo
	//del valor de la llave
	//para meter items dentro de un arreglo map, se hace instanciado el array con su llave
	//igual al valor
	


}
