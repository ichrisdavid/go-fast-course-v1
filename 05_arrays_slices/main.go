package main

import "fmt"



func main() {
	//Arrays
	//var fruitArr []string
	type Array struct {
		
	}

	//Assign values
/*
	fruitArr[0] = "Apple"
	fruitArr[1] = "Orange"
	fruitArr[2] = "h"
*/
	
	//declare in assign
	/*
	fruitArr := [2]string{"aplle","orange"}
	fmt.Println(fruitArr[1])
	*/

	fruitSlice := []string{"apple","orange","banana","piña"}
//	fmt.Println(fruitSlice[0:3])
	fruitSlice = append(fruitSlice,"algo")
//	fmt.Println(fruitSlice)
//si lo declaro con el tipo de variable antes como un var, es un arreglo y no es un slice
//el motodo append solo trabaja sobre slice para crear un slice uso el make asi: arrayTest := make([]string,3)
	//var arrayTest [2]string
	//la palabra reservada make me permite en esta ocacion crear un arreglo, recibe 2 parametros, el tipo de arreglo y el tamaño
	//
	arrayTest2 := make([]string,0)
	//arrayTest2[0] = "hola"
	//arrayTest2[1] = "1"
	//arrayTest2[2] = "mundo"
	arrayTest2 = append(arrayTest2,"x")
	fmt.Println(len(arrayTest2[0]))
	






}
